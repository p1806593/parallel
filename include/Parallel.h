//
// Created by constantin on 20/01/2021.
//

#ifndef ATRIA_PARALLEL_H
#define ATRIA_PARALLEL_H

#include "Job.h"
#include "Singleton.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <list>
#include <vector>
#include <memory>

class Parallel : public Singleton<Parallel>{
public:
    explicit Parallel(const int & tPoolSize=4);
    ~Parallel();
    void addJobToQueue(Job & j);
    void waitForJob();
    std::vector<Job *> jobQueue;
    std::vector<std::thread> workerThreadPool;
    bool WorkerIsRunning = true;
private:
    std::thread TimerThr;
    std::mutex TimerMtx, WorkerMtx, WaitMtx;
    std::condition_variable TimerCV, WorkerCV, WaitCV;

    void workerThread();

};

#endif //ATRIA_PARALLEL_H
