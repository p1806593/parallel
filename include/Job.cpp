//
// Created by constantin on 20/01/2021.
//

#include "Job.h"
#include "Parallel.h"

void Job::scheduleJob() {
    Parallel::GetInstance()->addJobToQueue(*this);
}
