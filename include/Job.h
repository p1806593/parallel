//
// Created by constantin on 20/01/2021.
//

#ifndef ATRIA_JOB_H
#define ATRIA_JOB_H
#include <initializer_list>
#include <iostream>
#include <filesystem>

struct Job {
    Job()=default;
    ~Job(){}
    virtual void work()=0;
    void scheduleJob();
};

struct AddHPJob : public Job{
public :
    int hp;
    void work() override{
        hp++;
    }
};

#endif //ATRIA_JOB_H
