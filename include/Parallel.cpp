//
// Created by constantin on 20/01/2021.
//

#include <iostream>
#include "Parallel.h"

Parallel::Parallel(const int & tPoolSize) {
    for (int i = 0; i <tPoolSize; ++i)
        workerThreadPool.emplace_back(&Parallel::workerThread, this);
    jobQueue.reserve(20000);
}

Parallel::~Parallel() {
    std::lock_guard<std::mutex> lck{WorkerMtx};
    WorkerIsRunning = false;
    WorkerCV.notify_all();
    for (auto &t : workerThreadPool)
        t.join();
}

void Parallel::addJobToQueue(Job &j) {
    std::lock_guard<std::mutex> lck{WorkerMtx};
    jobQueue.push_back(&j);
    WorkerCV.notify_one();
}

void Parallel::workerThread() {
    for (;;)
    {
        std::unique_lock<std::mutex> lck{WorkerMtx};
        WorkerCV.wait(lck, [this] { return !jobQueue.empty() ||
                                           !WorkerIsRunning; } );
        if (!WorkerIsRunning)
            break;
        auto &j = jobQueue.back();
        jobQueue.pop_back();
        j->work();
        lck.unlock();
        if (jobQueue.empty()){
            WaitCV.notify_all();
        }
    }
}

void Parallel::waitForJob() {
    if(!jobQueue.empty()){
        std::unique_lock<std::mutex> lck{WaitMtx};
        WaitCV.wait(lck,[this] { return jobQueue.empty();
                                                        });
        lck.unlock();
    }
}
