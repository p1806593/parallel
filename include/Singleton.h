//
// Created by constantin on 28/01/2021.
//

#ifndef PARALLEL_SINGLETON_H
#define PARALLEL_SINGLETON_H

/**
 * The Singleton class defines the `GetInstance` method that serves as an
 * alternative to constructor and lets clients access the same instance of this
 * class over and over.
 */
template<class T>
class Singleton
{

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */

protected:
    Singleton()=default;
    static T* singleton_;

public:
    Singleton(Singleton &other) = delete;
    void operator=(const Singleton &) = delete;

    static T *GetInstance(){
        if(singleton_==nullptr){
            singleton_ = new T();
        }
        return singleton_;
    }
};

template<class T>
T* Singleton<T>::singleton_= nullptr;



#endif //PARALLEL_SINGLETON_H
